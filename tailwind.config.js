module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      backgroundColor:{
        'balck-t-50':'rgba(0,0,0,0.5)',
        'green-t-50':'rgba(14, 99, 71, 0.5)',
      }
    },
  },
  variants:{},
  plugins: [],
}