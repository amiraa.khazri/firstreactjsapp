import axios from 'axios';
import React, { useState,useEffect } from 'react'
import HelloWord from './../Components/HelloWord';
import Loader from './../Components/Loader';
import ProductCard from './../Components/ProductCard';
import {useAxiosGet} from '../Hooks/HttpRequest';
export default function Home() {

  const url = `https://5e9623dc5b19f10016b5e31f.mockapi.io/api/v1/products?page=1&limit=10`
  
  let products = useAxiosGet(url)

 
  let content = null
  if (products.error) {
    content = <p>Please Try again ...</p>;
  }
  if (products.loading) {
    content = <Loader></Loader>;
  }

  if (products.data) {
    content = 
    products.data.map((product) =>
    <div key={product.id}>
      <ProductCard 
      product={product}
      />
    </div>
    
    )
  }

    return (
      <div>
          <HelloWord name="Amira" />
        <h1 className="font-bold text-2xl mb-3">Our Products, The Best Products</h1>
        {content}
      </div>
    );
}
