import React from "react";

export default function About() {
  return (
    <div>
      <h1 className="font-bold text-2xl mb-3">About Us</h1>
      <p>
        Your About Us page should be: Informative. It doesn’t always have to
        tell your whole story, but it should at least provide people with an
        idea of . Contain social proof, testimonials, and some personal
        information that visitors can relate to, such as education, family, etc.
        Useful. Engaging. Easy to navigate. Accessible on any device.
      </p>
    </div>
  );
}
