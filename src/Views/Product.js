import axios from "axios";
import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Loader from "./../Components/Loader";
import { useAxiosGet } from './../Hooks/HttpRequest';
export default function Product() {
  const { id } = useParams();
  const url = `https://5e9623dc5b19f10016b5e31f.mockapi.io/api/v1/products/${id}`;
  let product = useAxiosGet(url)

  let content = null;
 

  if (product.error) {
    content = <p>Please Try again ...</p>;
  }
  if (product.loading) {
    content = <Loader></Loader>;
  }
  if (product.data) {
    content = (
      <div>
        <h1 className="font-2xl font-bold mb-3">{product.data.name}</h1>
        <div>
          <img src={product.data.images[0].imageUrl} alt={product.data.name} />
        </div>
        <div className="font-xl font-bold mb-3">$ {product.data.price}</div>
        <div>{product.data.description}</div>
      </div>
    );
  }
  return (
    <div>
      <h1>{content}</h1>
    </div>
  );
}
