import React from 'react'

export default function Footer() {
    return (
        <footer className="bg-gray-200 bg-opacity-100 text-center text-xs p-3 absolute bottom-0 w-full">
            &copy; Copyright 2021 - Amira Khazri.
          
        </footer>
    )
}
