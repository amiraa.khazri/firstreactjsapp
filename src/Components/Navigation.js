import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { useTransition, animated } from "react-spring";
import { Route, BrowserRouter as Router, Switch, Link } from "react-router-dom";
import NavigationMenu from "./NavigationMenu";

export default function Navigation() {
  const [showMenu, setShowMenu] = useState(false);
  const maskTransitions = useTransition(showMenu, {
    from: { postion: "absolute", opacity: 0 },
    enter: { opacity: 1 },
    leave: { opacity: 0 },
  });

  const menuTransitions = useTransition(showMenu, {
    from: { opacity: 0, transform: "translateX(-100%)" },
    enter: { opacity: 1, transform: "translateX(0%)" },
    leave: { opacity: 0, transform: "translateX(-100%)" },
  });

  // className='fixed bg-white top-0 left-0 w-4/5 h-full z-50 shadow'
  // className='bg-balck-t-50 fixed top-0 left-0 w-full h-full z-50'

  return (
    <nav>
      <span className="text-xl">
        <FontAwesomeIcon icon={faBars} onClick={() => setShowMenu(!showMenu)} />
      </span>
      {maskTransitions(
        (props, item) =>
          item && (
            <animated.div
              style={props}
              className="bg-green-t-50 fixed top-0 left-0 w-full h-full z-50"
              onClick={() => setShowMenu(false)}
            >
              the menu
            </animated.div>
          )
      )}

      {menuTransitions(
        (props, item) =>
          item && (
            <animated.div
              style={props}
              className="fixed bg-white top-0 left-0 w-4/5 h-full z-50 shadow  p-3"
            >
            <NavigationMenu
              closeMenu={() => setShowMenu(false)}
             /> 
            </animated.div>
          )
      )}
    </nav>
  );
}
