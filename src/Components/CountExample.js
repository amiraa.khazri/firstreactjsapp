import React, { useState } from 'react'

export default function CountExample() {
    const [count, setCount] = useState(0)

    return (
        <div>
        <h1>
            {count}
        </h1>
        <h1 onClick={()=>setCount(count+1)}>
            Increment
        </h1>
            
        </div>
    )
}
